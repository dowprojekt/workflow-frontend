/**
 * @file workflows-service.js
 * 
 * This Service takes care of all operations related to workflows. - -
 * 
 * @@source_header
 * 
 * FIXME watch the application freshness
 * 
 */
'use strict';

(function() {

	/**
	 * Service singleton
	 */
	function WorfklowsService($q, $location, ApplicationStateService,
			ApplicationService, BackendService, UserService) {

		var _self = this;

		/**
		 * Variables for storing workflows and workflows-tags matches
		 */
		_self.workflows = {};
		_self.workflowsTagsMatches = {};

		/**
		 * Initalize the WorkflowsService
		 */
		_self.init = function() {
			return _getWorkflowsFromBackend();
		};

		/**
		 * Get a reference to the list of workflows
		 */
		_self.getListOfWorkflows = function() {
			return _self.workflows || {};
		};

		/**
		 * Get a reference to a specific workflow
		 * 
		 * @param wid
		 *          The Workflow ID of the workflow we want to get. If wid is equal
		 *          to 'current', the workflow ID is infered from the current path
		 *          (2nd level of the path without any other check)
		 */
		_self.getWorkflow = function(wid) {
			if (!wid && wid != 'current' && !wid.match(/^\d+$/)) {
				ApplicationStateService.crash('Invalid workflow specifier ' + wid);
				return false;
			}
			if (wid == 'current') {
				var path = $location.path();
				path = path.split('/');
				var wid = path[path.length - 2]; // the last element is *always the
																					// category
				if (!wid.match(/^\d+$/)) {
					ApplicationStateService.crash('Invalid workflow specifier ' + wid);
					return false;
				}
			}
			return _self.workflows[wid]
					|| ApplicationStateService.crash('Non-existent workflow.');
		}

		/**
		 * Do we have workflows to display?
		 */
		_self.hasWorkflows = function() {
			return (Object.keys(_self.workflows).length) ? true : false;
		};

		/**
		 * Get the reference to the matches between workflows and tags
		 */
		_self.getWorkflowsTagsMatches = function() {
			return _self.workflowsTagsMatches;
		}

		/**
		 * Gets or updates the workflows from the backend
		 */
		var _getWorkflowsFromBackend = function() {
			return BackendService.getWorkflows().then(function(workflows) {
				angular.copy(workflows, _self.workflows);

				_constructWorkflowsTagsMatches();

				angular.forEach(_self.workflows, function(w) {
					// is the workflow locked by myself? If that's the case, consider as unlocked.
					if (w.userLock && w.userLock == UserService.userAuthService.uid) { // FIXME requires UserService to work properly
						w.userLock = 0;
					}
					_self.mapScheduleSettingsFromBackend(w);
				});

			});
		};

		/**
		 * Contruct the object that contains references between workflows and tags
		 * 
		 * FIXME must be called if alteration on workflow list and workflow tags,
		 * but not otherwise / check if this is ok
		 */
		var _constructWorkflowsTagsMatches = function() {
			var tags = {};
			var unsorted = [];
			angular.forEach(_self.workflows, function(value, key) {
				if (value.tags) {
					angular.forEach(value.tags, function(v, ktag) {
						var tag = v.label;
						if (tags[tag]) {
							tags[tag].push(value);
						} else {
							tags[tag] = [ value ];
						}
					}, tags);
				} else {
					unsorted.push(value);
				}
			});

			if (unsorted) {
				tags.unsorted = unsorted;
			}
			angular.copy(tags, _self.workflowsTagsMatches);
		};

		/**
		 * Save a new or existing workflow
		 * 
		 * @return Returns the workflow id of the workflow
		 */
		_self.saveWorkflow = function(workflow) {
			return ApplicationService.createTags(workflow.tags).then(function() {
				// call the backend
				if (!workflow.id) { // new workflow
					return BackendService.createWorkflow(workflow)
					// keep the new workflow id for later
					.then(function(id) {
						workflow.id = id;
					});
				} else { // existing workflow
					return BackendService.updateWorkflow(workflow);
				}
			})
			// refresh the list of workflows
			.then(_self.init)
			// Return the saved workflow id
			.then(function() {
				return workflow.id;
			});
		};

		/**
		 * Clone a workflow: make a deep copy of the workflow identified by wid,
		 * then change its title and save the result in the backend.
		 */
		_self.cloneWorkflow = function(wid) {
			if (!_self.workflows[wid]) {
				ApplicationStateService.addMessage(
						'The specified workflow does not exist and cannot be cloned.',
						'warning');
				return; // promise rejection
			}

			var new_workflow = angular.copy(_self.workflows[wid]);
			delete new_workflow.id;
			new_workflow.name = 'Copy of ' + new_workflow.name;

			return BackendService.createWorkflow(new_workflow)
			// keep the new workflow id for later
			.then(function(id) {
				new_workflow.id = id;
			})
			// refresh the list of workflows
			.then(_self.init)
			// Return the saved workflow id
			.then(function() {
				return new_workflow.id;
			});
		}

		/**
		 * Delete a workflow
		 */
		_self.deleteWorkflow = function(wid) {
			if (!_self.workflows[wid]) {
				ApplicationStateService.addMessage(
						'The specified workflow does not exist and cannot be deleted.',
						'warning');
				return;
			}
			return BackendService.deleteWorkflow(wid).then(_self.init);
		};

		/**
		 * Lock a workflow
		 */
		_self.lock = function(wid) {
			// FIXME TODO
			// explicit locking of workflow by the user. To be implemented.
		};

		/**
		 * Unlock a workflow
		 */
		_self.unlock = function(wid) {
			return BackendService.deleteWorkflowLockStatus(wid).then(_self.init);
		};

		/**
		 * Tells if an action can be performed on a workflow
		 */
		_self.canDoAction = function(wid, action) {
			var w = _self.workflows[wid];
			if (action == 'run') {
				if (w.status == 1 || w.status == 4 || w.status == 5 || w.status == 6) {
					return true;
				}
			} else if (action == 'pause') {
				if (w.status == 3) {
					return true;
				}
			} else if (action == 'stop') {
				if (w.status == 2 || w.status == 3) {
					return true;
				}
			} else if (action == 'clone') {
				return true;
			} else if (action == 'delete') {
				if (!w.userLock) {
					return true;
				}
			}
			return false;
		};

		/**
		 * Trigger an action on a workflow
		 */
		_self.triggerAction = function(wid, action) {
			switch (action) {
			case 'view-details':
				$location.path('/workflow/' + wid);
				break;
			case 'run':
				BackendService.triggerWorkflowAction(wid, 'execute').then(function() {
					$location.path('/workflow/' + wid + '/execution');
				});
				break;
			case 'stop':
				BackendService.triggerWorkflowAction(wid, 'stop');
				break;
			case 'pause':
				BackendService.triggerWorkflowAction(wid, 'pause'); // FIXME not
																														// implemented??
				break;
			case 'delete':
				_self.deleteWorkflow(wid).then(
						function() {
							ApplicationStateService.addMessage('Workflow #' + wid
									+ ' has been deleted.', 'success', false);
							$location.path('/');
						});
				break;
			case 'clone':
				_self.cloneWorkflow(wid).then(
						function(new_id) {
							ApplicationStateService.addMessage('Workflow #' + wid
									+ ' has been cloned.', 'success', false);
							$location.path('/workflow/' + new_id);
						});
				break;
			default:
				break;
			}
		};

		/**
		 * Get a user-friendly string for statuses
		 */
		_self.getUserFriendlyStatus = function(status) {
			switch (status) {
			case 0:
				return 'CONFIGURATION';
			case 1:
				return 'READY';
			case 2:
				return 'QUEUED';
			case 3:
				return 'RUNNING';
			case 4:
				return 'STOPPED';
			case 5:
				return 'COMPLETED'
			case 6:
				return 'FAILED';
			default:
				return 'UNKNOWN';
			}
		};

		/**
		 * Helper function to map our frontend simple schedule data structe to the
		 * one of the backend.
		 * 
		 * We assume that the backend won't complain about our custom data structure
		 * (it will just ignore it), so we don't delete it.
		 */
		_self.mapScheduleSettingsToBackend = function(workflow) {
			workflow.executionSettings = {
				schedule : {
					intervalDay : false,
					intervalWeek : false,
					intervalMonth : false
				}
			};
			// frontend data structure
			// workflow.execution_frequency = hourly | daily | weekly | monthly |
			// manual
			switch (workflow.execution_frequency) {
			case 'daily':
				workflow.executionSettings.schedule.intervalDay = true;
				break;
			case 'weekly':
				workflow.executionSettings.schedule.intervalWeek = true;
				break;
			case 'monthly':
				workflow.executionSettings.schedule.intervalMonth = true;
				break;
			case 'manual':
			default:
				break;
			}
		};

		/**
		 * Helper function to map the backend schedule settings in our frontend
		 * custom data structure
		 * 
		 * We assume that the provided backend data structure is valid.
		 */
		_self.mapScheduleSettingsFromBackend = function(workflow) {

			/*
			 * Backend data structure: workflow.executionSettings = { schedule: {
			 * intervalDay: false, intervalWeek: false, intervalMonth: false } };
			 * 
			 * frontend data structure workflow.execution_frequency = hourly | daily |
			 * weekly | monthly | manual
			 */
			workflow.execution_frequency = '';
			if (workflow.executionSettings.schedule.intervalDay) {
				workflow.execution_frequency = 'daily';
			} else if (workflow.executionSettings.schedule.intervalWeek) {
				workflow.execution_frequency = 'weekly';
			} else if (workflow.executionSettings.schedule.intervalMonth) {
				workflow.execution_frequency = 'monthly';
			}
		};

		/**
		 * Get user-friendly version of the backend-encoded schedule definition
		 */
		_self.getUserFriendlyWorkflowSchedule = function(workflow) {
			if (workflow.executionSettings.schedule.intervalDay) {
				return 'daily';
			} else if (workflow.executionSettings.schedule.intervalWeek) {
				return 'weekly';
			} else if (workflow.executionSettings.schedule.intervalMonth) {
				return 'monthly';
			} else {
				return 'manually';
			}
		};

	}

	/**
	 * Init the module
	 */
	angular
			.module(
					'workflowEditor.workflowsService',
					[ 'workflowEditor.applicationStateService',
							'workflowEditor.applicationService',
							'workflowEditor.backendService', 'workflowEditor.userService' ])

			/**
			 * Instantiate the service.
			 */
			.service(
					'WorkflowsService',
					[ '$q', '$location', 'ApplicationStateService', 'ApplicationService',
							'BackendService', 'UserService', WorfklowsService ])

			/**
			 * workflows filter
			 * 
			 * A workflow object contains the following properties that are relevant
			 * for filtering:
			 *  - description
			 *   - name
			 *    - tags = [ {label: "name of tag"}, ... ] 
			 * - status = 0..6
			 * 
			 * @param items
			 *          Items may be an object of objects but we always return an
			 *          array of objects
			 */
			.filter(
					'workflows',
					[ function() {
						return function(items, text, status) {
							text = text || '';
							status = parseInt(status) || '';

							var filtered = [];
							angular.forEach(items, function(value, key) {
								var matching = true;

								if (text) {
									var pattern = RegExp(text, 'i')
									if (!pattern.test(value.name)
											&& !pattern.test(value.description)) {
										matching = false;

										angular.forEach(value.tags, function(t) {
											if (pattern.test(t.label)) {
												matching = true;
											}
										});
									}
								}

								if (matching && status !== '') {
									if (value.status !== status) {
										matching = false;
									}
								}

								if (matching) {
									filtered.push(value);
								}
							});
							return filtered;
						}
					} ]);

}());