// FIXME TODO DEBUG
describe('Service: BackendService', function() {
	beforeEach(module('workflowEditor.backendService'));
	
	var svc = false;;
	beforeEach(inject(function(BackendService) {
		svc = BackendService;
	}));
	
	// we should mock some calls to $http with $provide to 
	// be able to acquire fake data and still run our functions
	// backend-service-mocks.js is intended for our other modules, not 
	// for testing this service.
	
	it('Should load application status', function() {
		svc.loadApplicationStatus();
		expect(svc.application.runTime.status).toEqual('OK');
	});
	
});
