/**
 * @file user-service.js
 * 
 * This Service handles all operations related to users.
 * 
 * @@source_header
 * 
 */
'use strict';

(function() {

	/**
	 * Service singleton
	 */
	function UserService(BackendService) {
		
		var _self = this;
		
		/**
		 * An authentication user service, that must implement the following methods:
		 * - checkAuth(): return true if authentication is successful 
		 * - getRoles(): returns an array of {roleid: <string>, rolename: <string>} objects
		 * 		valid roleids for this application are 
		 * 		- admin
		 * 		- user
		 */
		_self.userAuthService = false;
		
		/** 
		 * Flag telling if the user is authenticated.
		 */
		_self.isAuthenticated = false;
		
		/**
		 * User roles
		 * 
		 * @private
		 * 
		 * @see hasRole(r)
		 */
		var roles = false;

		/**
		 * Initialize User Service
		 * 
		 * @return true if the user is authenticated, false otherwise.
		 */
		_self.init = function(externalAuthSvc) {
				_self.userAuthService = externalAuthSvc || _self;
				_self.isAuthenticated = _self.userAuthService.checkAuth();
				roles = _self.userAuthService.getRoles();
				return _self.isAuthenticated;
		};
		
		/**
		 * Dummy roles: we grand all roles
		 */
		_self.getRoles = function() {
			if (_self.isAuthenticated) {
				return {admin: 'Administrator', user: 'User'};
			}
			else {
				return false;
			}
		}
		
		/**
		 * Dummy authentication check
		 */
		_self.checkAuth = function() {
			return true;
		}
		
		/**
		 * Check whether the current user has the specified role.
		 * 
		 * @param role a role ID.
		 * @return true if the user has the role, false otherwise.
		 */
		_self.hasRole = function(role) {
			if (!roles) {
				return false;
			}
			for (var roleid in roles) {
				if (role === roleid) {
					return true;
				}
			}
			return false;
		};
		
		
	}

	/**
	 * Module definition
	 */
	angular.module('workflowEditor.userService',
			[ 'ngCookies', 'workflowEditor.applicationStateService', 'workflowEditor.backendService' ])
			
	/**
	 * Service instantiation
	 */
	.service('UserService', [ 'BackendService', UserService ])
	
	/**
		 * query interceptor: we add some authentication things in there.
		 * 
		 * FIXME this will be done by the external authentication service, if available
		 * we keep this interceptor for documentation purpose.
		 * 
		 */
	.factory('AuthenticationInterceptor',
			[ '$q', '$cookies', '$cookieStore', 'ApplicationStateService', 
			  function($q, $cookies, $cookieStore, ApplicationStateService) {
				return {
					/**
					 * Make sure credentials information are sent along with the request,
					 * even if we are not on the same server (CORS)
					 */
					request: function(config) {
						// required to send Cookie
						config.withCredentials = true;
						
						// FIXME see #10 - does not work as expected, we may move to Token auth
						if (config.url == 'loginurl') { // could be something like 
							//$cookies.sessionid = 'CHANGE'; // or 
							$cookieStore.put('sessionid', 'sessionidvalue');
						}
						return config;
					},
					/**
					 * If we get a 403 error, do crash
					 */
					responseError : function(rejection) {
						if (rejection.status === 403) {
							ApplicationStateService.crash('403 #2');
						}
						return $q.reject(rejection);
					}
				};
	} ])
				

	/**
	 * Add an interceptor for error handling
	 */
	.config([ '$httpProvider', function($httpProvider) {
			$httpProvider.interceptors.push('AuthenticationInterceptor');
	} ]);

}());
