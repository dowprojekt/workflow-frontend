/**
 * @file backend-service.js
 * 
 * This Service uses the REST API to retrieve and update the data model on the
 * backend.
 * 
 * NOTE FIXME all requests with session id from cookie.
 * FIXME do db input sanitization before passing to other services 
 * 
 * @@source_header
 * 
 */
'use strict';

(function() {

	/**
	 * Backend service singleton definition
	 */
	function BackendService($http, $q, ApplicationStateService, WORFLOW_EDITOR_CONFIG) {
		
		var _self = this;

		// ********************************** Application-related

		/**
		 * GET /application returns general application settings
		 */
		_self.getApplicationSettings = function() {
			return {}; // return FIXME TODO 
		};

		/**
		 * GET /application/seqno/ returns the latest server seqno so
		 * that the client knows whether updates are required
		 */
		_self.getLastApplicationSeqNo = function() {
			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'application/seqno').then(
					function(response) {
						return response.data;
					});
		};

		/**
		 * GET /application/status returns current application status
		 */
		_self.getApplicationStatus = function() {
			return { // FIXME TODO
				status : 'OK',
				feedback : 'Feedback',
				seq_no : 123
			// is that acquired here?
			};
		};

		/**
		 * GET /application/version returns the current API version
		 */
		_self.getAPIVersion = function() {
			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'application/version').then(
					function(response) {
						return response.data;
					});
		};

		// ********************************** Tags-related

		/**
		 * GET /application/tags returns a list of all tags (public)
		 */
		_self.getTags = function() {

			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'application/tags').then(
					function(response) {
						var ret = {};
						angular.forEach(response.data.content, function(value) {
							// value.tag constains: id, sn, parent, label
							// return { id: label, id2: label2, ...}
							ret[value.tag.id] = value.tag.label;
						});
						return ret;
					});
		};

		/**
		 * PUT /application/tags/{tag label} updates the tag with the given label
		 * 
		 * FIXME not used?
		 */
		_self.updateTag = function(label, new_label) {
			// put(label, new-label)
		};

		/**
		 * POST /application/tags creates a new tag
		 */
		_self.createTag = function(label) {
			var data = {label: label};
			return $http.post(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'application/tags', data);
		};

		/**
		 * DELETE /application/tags/{tag label} deletes the tag with the given label
		 * (needs to be url-encoded)
		 */
		_self.deleteTag = function(label) {

		};

		// ********************************** User-related

		/**
		 * GET /loggedinuser returns a user object for the currently logged in user
		 */
		_self.getLoggedInUserInfos = function() {

			return {
				uid : 123,
				name : 'John Smith', // FIXME from backend.
				roles : [ {
					rid : 3,
					name : 'Admin'
				}, {
					rid : 4,
					name : 'Editor'
				} ],
				preferences : {
					language : 'en',
					timezone : 'Europe/Berlin'
				},
				authenticationInfo : {
					token : 'random'
				}
			};
		};

		/**
		 * GET /user/{id} returns non-sensitive information about a certain user
		 * 
		 * @param the
		 *          id of the user for whom we want to get information
		 */
		_self.getUserPublicInfos = function(userid) {
			return {
				uid : 123,
				name : 'John Smith'
			};
		}

		// ********************************** Data function-related

		/**
		 * GET /datafunctions returns all data function objects
		 */
		_self.getDataFunctions = function() {
			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'datafunctions').then(
					function(response) {
						var ret = {};
						angular.forEach(response.data.content, function(value) {
							ret[value.dataFunction.id] = value.dataFunction;
						});
						return ret;
					}
			);
		};

		/**
		 * GET /datafunctions/{id} returns a specific data function object
		 */
		_self.getDataFunction = function(fid) {
			// FIXME input sanitization
			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'datafunctions/' + fid).then(
					function(response) {
						var ret = {};
						ret[response.data.dataFunction.id] = response.data.dataFunction;
						return ret;
					}
			);
		};

		/**
		 * PUT /datafunctions/{id} updates the datafunction with the given label
		 */
		_self.updateDataFunction = function(data) {
			// make sure we never pass the lock with the object
			delete data.userLock;
			return $http.put(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'datafunctions/' + data.id, data)
		};

		/**
		 * POST /datafunctions creates a new datafunction
		 */
		_self.createDataFunction = function(data) {
			// make sure we never pass the lock with the object
			delete data.userLock;
			return $http.post(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'datafunctions', data).then(function(response) { 
				var loc =  response.headers('location').split('/');
				return loc[loc.length - 1];
			}); 
		};

		/**
		 * DELETE /datafunctions/{id} deletes a specific data function object
		 */
		_self.deleteDataFunction = function(dfid) {
			return $http.delete(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'datafunctions/' + dfid);
		};

		/**
		 * /datafunctions/{id}/lock
		 *
		 * Delete a lock on a data function
		 */
		_self.deleteDataFunctionUserLock = function(dfid) {
			return $http.delete(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + '/datafunctions/' + dfid + '/lock');
		}
		
		// ********************************** Workflow-related
		/**
		 * GET /workflows returns a list of all workflows
		 */
		_self.getWorkflows = function() {
			return $http.get(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'workflows').then(
					function(response) {
						var ret = {};
						angular.forEach(response.data.content, function(value) {
							ret[value.workflow.id] = value.workflow;
						});
						return ret;
					}
			);
		};

		/**
		 * GET /workflows/{id} returns all static information about a specific workflow
		 */
		_self.getWorkflow = function(wid) {
			return {};
		};

		/**
		 * GET /workflows/{id}/executionstatus/ returns execution status information
		 */
		_self.getWorkflowStatus = function(wid) {
			return {};
		};

		/**
		 * GET /workflows/{id}/lockstatus returns workflow lockstatus incl user id
		 * who locked it
		 */
		_self.getWorkflowLockStatus = function(wid) {
			return {};
		};

		/**
		 * PUT /workflows/{id}/lockstatus changes the lockstatus of a specific
		 * workflow
		 */
		_self.updateWorkflowLockStatus = function(wid, obj) {

		}
		
		/**
		 * DELETE /workflows/{id}/lock 
		 * Delete a lock for a specific workflow
		 */
		_self.deleteWorkflowLockStatus = function(wid) {
			return $http.delete(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + '/workflows/' + wid + '/lock');
		};

		/**
		 * PUT /workflows/{id}/executionstatus/{operation} triggers execution
		 * related operations like run, ...
		 */
		_self.triggerWorkflowAction = function(wid, action) {
				return $http.put(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'workflows/' + wid + '/executionstatus/' + action);
		};

		/**
		 * POST /workflows creates a new workflow
		 *
		 */
		_self.createWorkflow = function(data) {
			// make sure we never pass the lock with the object
			delete data.userLock;
			return $http.post(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'workflows', data).then(function(response) { 
				var loc =  response.headers('location').split('/');
				return loc[loc.length - 1];
			}); 
		};
		
		/**
		 * PUT /workflows/{id} updates a specific workflow entirely
		 */
		_self.updateWorkflow = function (data) {
			// make sure we never pass the lock with the object
			delete data.userLock;
			return $http.put(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'workflows/' + data.id, data);
		}

		/**
		 * DELETE /workflows/{id} deletes a specific workflow
		 */
		_self.deleteWorkflow = function(wid) {
			return $http.delete(WORFLOW_EDITOR_CONFIG.BACKEND_ENTRY_POINT + 'workflows/' + wid);
		};

		/**
		 * DELETE /workflows/{id}/datafunction/{id} deletes a specific data function
		 * from a specific workflow
		 */
		_self.deleteDataFunctionInWorkflow = function(wid, fid) {

		};

	
	}

	angular.module('workflowEditor.backendService', ['workflowEditor.config', 'workflowEditor.applicationStateService'])
		/** 
		 * Define the error handling interceptor
		 */
		.factory('ErrorHandlingInterceptor', ['$q', 'ApplicationStateService', function($q, ApplicationStateService) {
			return {
				/**
				 * Detect errors on responses and announce a crash.
				 */
				responseError: function(rejection) {
					if (rejection.status === 500 || rejection.status === 404) {
						ApplicationStateService.crash(rejection);
					}
					return $q.reject(rejection);
				}
			};
		}])
		/**
		 * Add an interceptor for error handling
		 */
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.interceptors.push('ErrorHandlingInterceptor')
		}])
		/**
		 * Register Backend interaction service
		 */
		.service('BackendService',[ '$http', '$q', 'ApplicationStateService', 'WORFLOW_EDITOR_CONFIG', BackendService ]);

}());
