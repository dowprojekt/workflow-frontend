/**
 * @file application-service.js
 * 
 * This service provides application-wide settings
 * 
 */
'use strict';

(function() {

	/**
	 * Service singleton definition
	 */
	function ApplicationService($location, $q, ApplicationStateService,
			WORFLOW_EDITOR_CONFIG, BackendService, UserService) {

		var _self = this;

		/**
		 * Data structure storing application informations
		 */
		_self.application = {
			settings : {},
			runTime : {},
			tags : {},
			dataFunctions : {},
			seq_no : 0
		};

		// ********************************** Initialization

		/**
		 * Initialize the Application Service
		 */
		_self.init = function() {
			_initApplicationSettings();
			_initApplicationStatus();
			_initTags();
			_initDataFunctions();

			// FIXME if not acquired by _initApplicationStatus() ???
			_self.application.seq_no = BackendService.getLastApplicationSeqNo();
		};

		/**
		 * Check if our application is compatible with the API version
		 */
		_self.checkAPIVersion = function() {
			return BackendService.getAPIVersion().then(
					function(version) {
						if (version != WORFLOW_EDITOR_CONFIG.EXPECTED_API_VERSION) {
							ApplicationStateService.crash('API version mismatch: ' + version
									+ ' != ' + WORFLOW_EDITOR_CONFIG.EXPECTED_API_VERSION);
						}
					});
		}

		/**
		 * Gets or refreshes application-wide settings
		 */
		var _initApplicationSettings = function() {
			var ret = BackendService.getApplicationSettings();
			angular.copy(ret, _self.application.settings);
		};

		/**
		 * Gets or refreshes application status
		 */
		var _initApplicationStatus = function() {
			var ret = BackendService.getApplicationStatus();
			angular.copy(ret, _self.application.runTime);
		};

		/**
		 * Get last application transaction number and re-init the application if we
		 * do not have the latest version
		 */
		_self.checkApplicationFreshness = function() {
			var new_val = BackendService.getLastApplicationSeqNo();
			if (new_val > _self.application.seq_no) {
				_self.init();
			}
			_self.application.seq_no = new_val;
		};

		/**
		 * Gets or refreshes list of application tags
		 */
		var _initTags = function() {
			return BackendService.getTags().then(function(tags) {
				angular.copy(tags, _self.application.tags);
			});
		};

		/**
		 * Get or refreshes data functions list
		 */
		_self.initDataFunctions = function() {
			return BackendService.getDataFunctions().then(function(dataFunctions) {
				angular.copy(dataFunctions, _self.application.dataFunctions);

				angular.forEach(_self.application.dataFunctions, function(df) {
					// is the data function locked by myself? If that's the case, consider
					// as unlocked.
					
					// FIXME requires UserService with real Authentication facility to
					// work properly
					if (df.userLock && df.userLock == UserService.userAuthService.uid) {
						df.userLock = 0;
					}
				});

			});
		};
		var _initDataFunctions = _self.initDataFunctions; // alias

		/**
		 * Inspect a list of tags and create them if they do not exist. If a new tag
		 * is created, the provided object is updated with the id of the newly
		 * created tags
		 * 
		 * @return a promise
		 */
		_self.createTags = function(tags) {
			// first, create new tags
			var promises = [];
			for (var i = 0; i < tags.length; ++i) {
				if (!tags[i].id || _self.getTagName(tags[i].id) === false) {
					// create a new tag and a corresponding promise
					var p = _self.createTag(tags[i].label);
					promises.push(p);
				}
			}

			// when all tags have been successfully created, then create the workflow
			return $q.all(promises).then(
					function() {
						// we have created new tags, then assign their new ids to their
						// respective labels.
						if (promises.length) {
							for (var i = 0; i < tags.length; ++i) {
								if (!tags[i].id) {
									tags[i].id = _self.getTagId(tags[i].label)
											|| ApplicationStateService
													.crash('Failed to create and use a tag');
								}
							}
						}
					});
		}

		/**
		 * Create a new tag
		 * 
		 * @param name
		 *          the user-friendly name of the tag
		 * 
		 * @return the new tag id as returned by the backend.
		 */
		_self.createTag = function(name) {
			return BackendService.createTag(name).then(_initTags);
		};

		/**
		 * Get list of tags
		 */
		_self.getTags = function() {
			return _self.application.tags;
		};

		/**
		 * Get a tag label by specifying its id
		 */
		_self.getTagName = function(id) {
			return _self.application.tags[id] || false;
		};

		/**
		 * Get tag id by specifying its label
		 */
		_self.getTagId = function(name) {
			for ( var k in _self.application.tags) {
				if (_self.application.tags[k] === name) {
					return k;
				}
			}
		};

		/**
		 * Transform tags to a data structure that is compatible with ngTagsInput
		 * 
		 */
		_self.tagsToNgTags = function(tags) {
			var _tags_ng = [];
			angular.forEach(tags, function(value) {
				_tags_ng.push({
					label : value.label,
					id : value.id
				});
			});
			return _tags_ng;
		};

		/**
		 * Get tags in a ngTagsInput-compatible form.
		 * 
		 * @param optional
		 *          filter: only tags matching the provided filter will be returned
		 */
		_self.getNgTagsInputTags = function(query) {
			query = query || '';
			var ret = [];
			angular.forEach(_self.application.tags, function(value, key) {
				if (query && value.toLowerCase().indexOf(query.toLowerCase()) !== 0) {
					return;
				}
				// if an id property exists, it will be kept in the response when the
				// tag
				// is being selected by the user. So this is an easy way to determine
				// whether the user supplied a brand new tag or if he used one of those
				// that were provided by the backend.
				this.push({
					label : value,
					id : key
				});
			}, ret);
			return ret;
		}

		/**
		 * Get a specific data function
		 * 
		 * @param which
		 *          either the data function id, or 'current' if the data function
		 *          id must be infered from the current path
		 */
		_self.getDataFunction = function(which) {
			if (!which && which != 'current' && !which.match(/^\d+$/)) {
				ApplicationStateService.crash('Invalid data function specifier '
						+ which);
				return false;
			}
			if (which == 'current') {
				var path = $location.path();
				path = path.split('/');
			// the last element is *always* 'edit'
				var which = path[path.length - 2]; 
				if (!which.match(/^\d+$/)) {
					ApplicationStateService.crash('Invalid data function specifier '
							+ which);
					return false;
				}
			}
			return _self.application.dataFunctions[which]
					|| ApplicationStateService.crash('Non-existent data function.');
		};

		/**
		 * Save a new or update an existing data function.
		 */
		_self.saveDataFunction = function(df) {
			return _self.createTags(df.tags).then(function() {
				// call the backend
				if (!df.id) { // new data function
					return BackendService.createDataFunction(df)
					// keep the new id for later
					.then(function(id) {
						df.id = id;
					});
				} else { // existing data function
					return BackendService.updateDataFunction(df);
				}
			})
			// refresh the list of data function
			.then(_self.initDataFunctions)
			// Return the saved data function id
			.then(function() {
				return df.id;
			});
		};

		/**
		 * Clone a data function
		 */
		_self.cloneDataFunction = function(dfid) {
			if (!_self.application.dataFunctions[dfid]) {
				ApplicationStateService.addMessage(
						'The specified data function does not exist and cannot be cloned.',
						'warning');
				return; // promise rejection
			}

			var new_df = angular.copy(_self.application.dataFunctions[dfid]);
			delete new_df.id;
			new_df.name = 'Copy of ' + new_df.name;

			return BackendService.createDataFunction(new_df)
			// keep the new data function id for later
			.then(function(id) {
				new_df.id = id;
			})
			// refresh the list of data functions
			.then(_self.initDataFunctions)
			// Return the saved data function id
			.then(function() {
				return new_df.id;
			});

		};

		/**
		 * Delete a data function
		 */
		_self.deleteDataFunction = function(dfid) {
			if (!_self.application.dataFunctions[dfid]) {
				ApplicationStateService
						.addMessage(
								'The specified data function does not exist and cannot be deleted.',
								'warning');
				return;
			}
			return BackendService.deleteDataFunction(dfid).then(
					_self.initDataFunctions);
		};

		/**
		 * Lock (user-lock) a data function
		 */
		_self.lockDataFunction = function(dfid) {
			// FIXME TODO
		};

		/**
		 * Unlock a data function
		 */
		_self.unlockDataFunction = function(dfid) {
			return BackendService.deleteDataFunctionUserLock(dfid).then(
					_self.initDataFunctions);
		}

		/**
		 * Get a user-friendly name for the specified service type
		 */
		_self.getUserFriendlyServiceType = function(t) {
			switch (t) {
			case 1:
				return 'WSDL';
			case 2:
				return 'REST';
			case 3:
				return 'Application';
			default:
				return 'UNKNOWN';
			}
		}

		/**
		 * Trigger an action a specific data function
		 */
		_self.triggerDataFunctionAction = function(dfid, action) {
			switch (action) {
			case 'view-details':
				$location.path('/application/datafunction/' + dfid + '/edit')
				break;
			case 'delete':
				_self.deleteDataFunction(dfid).then(
						function() {
							ApplicationStateService.addMessage('Data function #' + dfid
									+ ' has been deleted.', 'success', false);
							$location.path('/application/datafunction/list');
						});
				break;
			case 'clone':
				_self.cloneDataFunction(dfid).then(
						function(new_id) {
							ApplicationStateService.addMessage('Data function #' + dfid
									+ ' has been cloned.', 'success', false);
							$location.path('/application/datafunction/' + new_id + '/edit');
						});
				break;
			default:
				break;
			}
		};

	}

	/**
	 * Initialize the module.
	 */
	angular.module(
			'workflowEditor.applicationService',
			[ 'workflowEditor.config', 'workflowEditor.applicationStateService',
					'workflowEditor.backendService', 'workflowEditor.userService' ])

	/**
	 * Instantiate the service.
	 */
	.service(
			'ApplicationService',
			[ '$location', '$q', 'ApplicationStateService', 'WORFLOW_EDITOR_CONFIG',
					'BackendService', 'UserService', ApplicationService ])

	/**
	 * datafunctions filter
	 * 
	 * A data function object contains the following properties that are relevant
	 * for filtering: 
	 * - description 
	 * - name 
	 * - tags = [ {label: "name of tag"}, ... ] 
	 * - types = 0..2
	 * 
	 * @param items
	 *          Items may be an object of objects but we always return an array of
	 *          objects
	 */
	.filter('datafunctions', [ function() {
		return function(items, text, type) {
			text = text || '';
			type = parseInt(type) || '';

			var filtered = [];
			angular.forEach(items, function(value, key) {
				var matching = true;

				if (text) {
					var pattern = RegExp(text, 'i')
					if (!pattern.test(value.name) && !pattern.test(value.description)) {
						matching = false;

						angular.forEach(value.tags, function(t) {
							if (pattern.test(t.label)) {
								matching = true;
							}
						});
					}
				}

				if (matching && type !== '') {
					if (value.type !== type) {
						matching = false;
					}
				}

				if (matching) {
					filtered.push(value);
				}
			});
			return filtered;
		}
	} ]);

}());