/**
 * @file application-state-service.js
 * 
 * This service provides information about the current state of the application.
 * Its purpose is to allow us to display such informations to the end user 
 * through the ApplicationState controller and submit informations that must
 * be shown to the user:
 * 
 * - if the application is crashed or not, and a mean to announce a crash
 * - messages to the user (info, warning, danger, success)
 * 
 * @@source_header
 * .
 * 
 */
'use strict';

(function() {

	/**
	 * Service singleton.
	 * 
	 * @returns
	 */
	function ApplicationStateService($rootScope, $routeParams) {
		
		var _self = this;
		
		/**
		 * Data structure containing status information
		 */
		_self.status = {
				crashed: false,
				message: ''
		};
		
		/**
		 * Messages types
		 * 
		 * @private
		 */
		var _messages_types = ['danger', 'warning', 'info', 'success'];
		
		/**
		 * Data structure containing messages
		 */
		_self.messages = {};
		angular.forEach(_messages_types, function(t) {
			_self.messages[t] = [];
		});
		
		/**
		 * Messages visibility states
		 */
		_self.messages_visibility = {
				any: false
		};
		angular.forEach(_messages_types, function(t) {
			_self.messages_visibility[t] = false;
		});
		
		
		/**
		 * When we change the current $route, delete non-persistent messages
		 */
		$rootScope.$on('$routeChangeSuccess', function() {
			_purge_non_persistent_messages();
		});
		
		
		/**
		 * Register a crash
		 */
		_self.crash = function(msg) {
			_self.status.crash = true;
			_self.status.message = msg || '';
			console.log(msg);
			throw new Error(msg);
		};
		
		/**
		 * Add a message
		 * 
		 * A persistent message will be kept open until the user clicks the "close" 
		 * button, whereas an non-persistent message will be closed implicitely if
		 * the user moves to another page.
		 */
		_self.addMessage = function(msg, type, persistent) {
			var type = type || 'info';
			var persistent = persistent || false;
			// check that it does not exist yet
			var already_exists = false;
			for(var i=0; i < _self.messages[type].length; ++i) {
				if (_self.messages[type][i].msg === msg) {
					already_exists = true;
					_self.messages[type][i].time = new Date().getTime();
					break;
				}
			}
			if (!already_exists) {
				_self.messages[type].push({
						msg: msg, 
						persistent: !!persistent,
						time: new Date().getTime()
				});
				_update_visibility();
			}
		};
		
		/**
		 * Reset (delete) messages of a given type
		 */
		_self.resetMessages = function(type) {
			if (type) {
				_self.messages[type] = [];
			}
			else {
				angular.forEach(_messages_types, function(t) {
					_self.messages[t] = [];
				});
			}
			_update_visibility();
		};
		
	
		/**
		 * Update message visibility 
		 * 
		 * @private
		 */
		var _update_visibility = function() {
			var any = false;
			angular.forEach(_messages_types, function(t) {
				var tmp = !!_self.messages[t].length;
				any |= tmp;
				_self.messages_visibility[t] = tmp;
			});
			_self.messages_visibility.any = any;
		}
		
		/**
		 * Remove non persistent messages
		 * 
		 * We do not delete messages that don't exist for more than 1500ms, because
		 * we may just have been redirected from another page and this function is
		 * called from the $routeChangeSuccess event
		 * 
		 * @private
		 */
		var _purge_non_persistent_messages = function()
		{
			angular.forEach(_messages_types, function(t) {
				for (var i=_self.messages[t].length-1; i >= 0; --i) {
					if (!_self.messages[t][i].persistent && _self.messages[t][i].time < new Date().getTime() - 1500) {
						_self.messages[t].splice(i,1);
					}
				}
			});
			_update_visibility();
		}
		
	}

	/**
	 * Init this module
	 */
	angular.module(
			'workflowEditor.applicationStateService',['ngRoute'])
	
	/**
	 * and instantiate this service.
	 */
	.service('ApplicationStateService',['$rootScope', '$routeParams', ApplicationStateService]);

}());