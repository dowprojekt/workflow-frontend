// FIXME TODO DEBUG
//
// not used as we didn't write unit tests
// however we may rather consider "Integration-level unit tests" 
// see "AngularJS Up & Running" by Shyam Seshadri & Brad Green, chapter 7 (p. 118)

'use strict';

(function() {
  
  function BackendService() {
    
    var _self = this;
    
    this.application = {};
    this.userInformation = {};
    this.workflows = {};
    this.dataFunctions = {};
    this.tags = {};
    
    // mock all functions
    _self.init = function() {
      _self.getWorkflowsList();

    };
    

    _self.getWorkflowsList = function() {
      var fake_workflows = {
          '123': {'id': 123, 'name': 'My Test workflow'},
          '2': {'id': 2, 'name': 'My Test workflow 2'}
      };
      _self.workflows = fake_workflows;
    };
    
    // FIXME implement ALL functions that exist in the original service
  }
  
  angular.module('workflowEditor.backendServiceMocks', [])
    .service('BackendService', [BackendService]);
  
}());
