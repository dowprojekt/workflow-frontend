/**
 * @file application-state.js
 * 
 * ApplicationStateController
 * 
 * Responsible for displaying application-wide state information 
 * - current status: crashed or not
 * - modal informational messages
 * 
 * @@source_header
 */
'use strict';

(function() {

	angular.module('workflowEditor.applicationState',
			[ 'workflowEditor.applicationStateService' ])

	.controller('ApplicationStateController',
			[ 'ApplicationStateService', function(ApplicationStateService) {
				
				var _self = this;

				/**
				 * Reference to the responsible service.
				 */
				_self.stateSvc = ApplicationStateService;

				/**
				 * Close alert messages
				 */
				_self.closeAlert = function(type) {
					_self.stateSvc.resetMessages(type);
				};

			} ]);

}());