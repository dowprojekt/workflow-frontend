/**
 * @file workflow-detail-header.js
 * 
 * workflowEditor.workflowDetailHeader module
 * 
 * This module renders the header of the pages allowing to view and edit the
 * details of a specific workflow.
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {

	angular.module('workflowEditor.workflowDetailHeader',
			[ 'workflowEditor.workflowsService' ])

	.controller('WorkflowDetailHeaderController',
			[ '$location', 'WorkflowsService', function($location, WorkflowsService) {

				var _self = this;

				/**
				 * Current workflow
				 */
				_self.workflow = WorkflowsService.getWorkflow('current');
				
				/**
				 * Reference to Workflows service.
				 */
				_self.workflowsService = WorkflowsService;

				/**
				 * Current section
				 */
				var path = $location.path();
				path = path.split('/');
				_self.section = path[path.length-1];
				
				/**
				 * Dialog for unlocking a workflow
				 */
				_self.requestWorkflowUnlock = function() {					
					WorkflowsService.unlock(_self.workflow.id).then(function() {
						_self.display_unlock_workflow = false;
					});
				}

			} ]);

}());
