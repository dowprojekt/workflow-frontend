/**
 * @file system-status.js
 * 
 * A widget that displays the current application status.
 * 
 * @@source_header
 * 
 */

'user strict';

(function() {

	angular.module('workflowEditor.systemStatus',
			[ 'workflowEditor.backendService' ])

	.controller('SystemStatusController',
			[ '$scope', 'ApplicationService', function($scope, ApplicationService) {

				var _self = this;
				
				/**
				 * Reference to the object that contains information about the application run-time.
				 */
				_self.status = ApplicationService.application.runTime;

				/**
				 * Register a watch on the status object
				 * 
				 * If the application status changes, update the displayed icon
				 */
				$scope.status = _self.status;
				$scope.$watch('status.status', function() {
					_refreshIcon();
				});

				/**
				 * Refresh the icon
				 * 
				 * @private
				 */
				var _refreshIcon = function() {
					switch (_self.status.status) {
					case 'OK':
						_self.iconIdentifier = 'check';
						break;
					case 'KO':
						_self.iconIdentifier = 'close';
						break;
					}
				};

			} ]);

}());
