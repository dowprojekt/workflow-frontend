/**
 * @file menu.js
 * 
 * workflowEditor.menu module
 * 
 * Responsible for rendering the menu on the left side of the screen
 * 
 * @@source_header
 */
'use strict';

(function() {
	
	angular.module('workflowEditor.menu', 
			['workflowEditor.applicationService', 'workflowEditor.workflowsService'])
	
	/**
	 * MenuController:
	 */
	.controller('MenuController',
			['ApplicationService', 'WorkflowsService', 'UserService', 
			 function (ApplicationService, WorkflowsService, UserService) {
		
		var _self = this;
		
		/**
		 * Controller initialization
		 */
		var _init = function() {
			_self.workflows = WorkflowsService.getListOfWorkflows();
			_self.workflowsTagsMatches = WorkflowsService.getWorkflowsTagsMatches();
		};
		
		/**
		 * Filter form input variables
		 */
		_self.input = {
				filter_workflows: ''
		};
		
		// run
		_init(); 

		/**
		 * Tells whether the user is an admin
		 */
		_self.isAdmin = !!UserService.hasRole('admin');

		/**
		 * Initialize default visibility of menus
		 */
		_self.visibility = {
				'general': true,
				'workflows': true
		};
		
		/**
		 * Variable telling which workflow is hovered, such that we can hover
		 * other instances of the same workflow that may appear in the listing.
		 * 
		 * This highlights the fact that these different occurences are in fact the
		 * same workflow.
		 */
		_self.hovered_workflow = '';
	
		/**
		 * Toggle visibility of a menu element
		 */
		_self.toggleVisibility = function(what) {
			_self.visibility[what] = !_self.visibility[what];
		};
		
		/**
		 * Get the chevron icon for a given menu element
		 * 
		 * @param what
		 * @param level
		 * @returns appropriate classes
		 */
		_self.getVisibilityClass = function(what, level) {
			level = typeof level !== 'undefined' ? level : 1;
			if (_self.visibility[what]) {
				return level == 1 ? 
						'glyphicon glyphicon-chevron-down' : 'fa fa-minus-square-o';
			}
			else {
				return level == 1 ? 
						'glyphicon glyphicon-chevron-up' : 'fa fa-plus-square-o';
			}
		};
		
	}]);

}());