// FIXME DEBUG TODO

'use strict';

describe('Controller: MenuController', function() {
  
  beforeEach(module('workflowEditor.menu'));
  beforeEach(module('workflowEditor.backendServiceMocks'));
  
  var ctrl = false, backendSvc = false;
  
  beforeEach(inject(function($controller, BackendService) {
    ctrl = $controller('MenuController');
    backendSvc = BackendService;
  }));
  
  beforeEach(function() {
    backendSvc.init();
  });
  
  it('should load workflow objects correctly', function() {
    expect(ctrl.workflows).toEqual({
      '123': {'id': 123, 'name': 'My Test workflow'},
      '2': {'id': 2, 'name': 'My Test workflow 2'}
    });
  });
  
  it('should handle visibility correctly', function() {
    // Default 
    angular.forEach(['general', 'workflows', 'workflows.misc'], 
        function(val) {
          expect(ctrl.visibility[val]).toBeTruthy();
          ctrl.toggleVisibility(val);
          expect(ctrl.visibility[val]).toBeFalsy();
          ctrl.toggleVisibility(val);
          expect(ctrl.visibility[val]).toBeTruthy();
        }
    );
   
  });
  
  it('should get chevron classes', function() {
    expect(ctrl.getVisibilityClass('general'))
      .toEqual('glyphicon glyphicon-chevron-down');
    ctrl.toggleVisibility('general');
    expect(ctrl.getVisibilityClass('general'))
      .toEqual('glyphicon glyphicon-chevron-up');
    ctrl.toggleVisibility('general');
    
    expect(ctrl.getVisibilityClass('general', 1))
      .toEqual('glyphicon glyphicon-chevron-down');
    ctrl.toggleVisibility('general');
    expect(ctrl.getVisibilityClass('general', 1))
      .toEqual('glyphicon glyphicon-chevron-up');
    ctrl.toggleVisibility('general');
    
    expect(ctrl.getVisibilityClass('general', 2))
      .toEqual('fa fa-minus-square-o');
    ctrl.toggleVisibility('general');
    expect(ctrl.getVisibilityClass('general', 2))
      .toEqual('fa fa-plus-square-o'); 

  });
});

