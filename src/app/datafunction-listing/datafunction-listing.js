/**
 * @file datafunction-listingjs
 * 
 * workflowEditor.dataFunctionListing module
 * 
 * This Controller will contain the list of all data functions.
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular.module(
			'workflowEditor.dataFunctionListing',
			[ 'workflowEditor.applicationStateService',
					'workflowEditor.applicationService'
					])

	.controller(
			'DataFunctionListingController',
			[
					'ApplicationStateService',
					'ApplicationService',
					function(ApplicationStateService, ApplicationService)  {

						var _self = this;

						/**
						 * Reference to the ApplicationService for being used from within the template.
						 */
						_self.applicationService = ApplicationService;
					
						/**
						 * Form input variables
						 * 
						 * input: the values being captured by the form
						 * applied_input_values: values being used for applying the filter
						 * on data functions
						 * 
						 */
						_self.input = {
							search : '',
							service_type : ''
						};
						_self.applied_input_values = angular.copy(_self.input);

						/**
						 * Do apply the filter
						 */
						_self.filterList = function() {
							_self.applied_input_values = angular.copy(_self.input);
						};

						/**
						 * Reset the filter to empty values
						 */
						_self.resetListFilter = function() {
							_self.input = angular.copy(_self.applied_input_values);
						};

						/**
						 * Tells whether the filter form is pristine or not
						 */
						_self.filterFormIsPristine = function() {
							return angular.equals(_self.input, _self.applied_input_values);
						}
						
					} 
			]);

}());
