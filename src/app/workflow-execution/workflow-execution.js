/**
 * @file workflow-execution.js
 * 
 * workflowEditor.execution module
 * 
 * This page displays the possible Execution actions (Run, Stop, Pause) and the 
 * execution history of a workflow.
 * 
 * @@source_header
 * 
 */

'use strict';
(function() {
	angular.module(
			'workflowEditor.execution',
			[ 'workflowEditor.applicationStateService',
					'workflowEditor.workflowsService' ])

	.controller(
			'ExecutionWorkflowController',
			[ '$location', 'ApplicationStateService', 'WorkflowsService',
					function($location, ApplicationStateService, WorkflowsService) {

						var _self = this;

						/**
						 * Reference to the workflow being worked on
						 */
						_self.workflow = WorkflowsService.getWorkflow('current');

						/**
						 * Reference to the Workflows service that will be used in the
						 * template
						 */
						_self.workflowsService = WorkflowsService;

					} ]);
}());
