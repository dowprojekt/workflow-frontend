/**
 * @file config.js
 * 
 * This module contains application-wide configuration, such as the backend 
 * entry point and the expected API version for the application to run
 * properly.
 * 
 * @@source_header
 * .
 * 
 */
'use strict';

(function() {

	
	/**
	 * Init this module
	 */
	angular.module(
			'workflowEditor.config',[])
			
	/**
	 * Constants
	 */
	.constant('WORFLOW_EDITOR_CONFIG', {
		BACKEND_ENTRY_POINT: 'http://localhost:8088/',
		EXPECTED_API_VERSION: '0.13'
	});

}());
