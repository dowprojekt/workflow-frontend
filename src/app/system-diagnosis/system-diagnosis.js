/**
 * @file system-diagnosis.js
 * 
 * Angular main module definition for our Workflow Editor application. 
 * This is the entry point.
 * 
 * @@source_header
 */

'use strict';

(function() {

	angular.module('workflowEditor.system', ['workflowEditor.workflowsService'])

	/**
	 * Definition of the SystemController that will contain the model for the 
	 * diagnosis view.
	 */
	.controller('SystemController', ['WorkflowsService', function(WorkflowsService) {

		var _self = this;
		
		
		/**
		 * FIXME DEBUG code
		 */
		
		_self.workflowsService = WorkflowsService;
		
		this.debugRunWorkflow = function() {
			WorkflowsService.triggerAction(_self.debug_workflow_id, 'run');
		}
	}]);

}());
