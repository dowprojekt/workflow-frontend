/**
 * @file datafunction-edit.js
 * 
 * workflowEditor.dataFunctionEdit module
 * 
 * Controller that will display the data function edition form
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular
			.module(
					'workflowEditor.dataFunctionEdit',
					[ 'workflowEditor.applicationStateService',
							'workflowEditor.applicationService' ])

			/**
			 * controller definition
			 */
			.controller(
					'EditDataFunctionController',
					[
							'$location',
							'ApplicationStateService',
							'ApplicationService',
							function($location, ApplicationStateService, ApplicationService) {

								var _self = this;

								/**
								 * Current workflow
								 */
								_self.df = ApplicationService.getDataFunction('current');
				
								/**
								 * Reference to the Application Service that
								 * expose methods that are used by the template
								 */
								_self.applicationService = ApplicationService;

								/**
								 * Current values of the edit input form
								 */
								_self.input_edit = angular.copy(_self.df);
								
								/**
								 * Original values that were fetched form the backend when
								 * loading the page.
								 * 
								 * @private
								 */
								var original_input_edit = angular.copy(_self.input_edit);

								/**
								 * Save the new information
								 * 
								 */
								_self.saveDataFunctionInfos = function() {
									ApplicationService.saveDataFunction(_self.input_edit).then(
											function(id) {
												ApplicationStateService.addMessage(
														'The new information has been saved.', 'success')
											});
								};
								
								/**
								 * Add a new parameter
								 */
								_self.addParameter = function() {
									_self.input_edit.parameters.push({
										type : 'string',
										name : '',
										defaultValue : '',
										mandatory : false,
									});
								};

								/**
								 * Remove a parameter
								 */
								_self.removeParameter = function(index) {
									_self.input_edit.parameters.splice(index, 1);
								};
								

								/**
								 * Reset the edit form to its initial state
								 */
								_self.resetEditionForm = function() {
									_self.input_edit = angular.copy(original_input_edit);
								};

								/**
								 * Tells whether the edit form has been modified.
								 */
								_self.editFormIsPristine = function() {
									return angular.equals(_self.input_edit, original_input_edit);
								}

								/**
								 * Trigger workflow deletion
								 * 
								 */
								_self.deleteDataFunction = function() {
									ApplicationService.deleteDataFunction(_self.df.id).then(
											function() {
												ApplicationStateService.addMessage('Data function #'
														+ _self.df.id + ' has been deleted.',
														'success', false);
												$location.path('/application/datafunction/list');
											});
								};

								/**
								 * Cancel the workflow deletion (uncheck the confirmation
								 * checkbox)
								 */
								_self.cancelDeletion = function() {
									_self.input_deletion.confirm_deletion = false;
								};

							} ]);

}());
