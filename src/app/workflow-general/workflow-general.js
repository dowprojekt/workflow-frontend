/**
 * @file workflow-info.js
 * 
 * workflowEditor.info module
 * 
 * Edition of general information about a workflow. This page consists of 2
 * forms: 
 * - workflow general info edition form 
 * - workflow deletion form
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular.module('workflowEditor.info', ['workflowEditor.applicationStateService', 'workflowEditor.applicationService', 'workflowEditor.workflowsService'])

	.controller(
			'InfoWorkflowController',
			[ '$location', 'ApplicationStateService', 'ApplicationService', 'WorkflowsService',
					function($location, ApplicationStateService, ApplicationService, WorkflowsService) {

						var _self = this;

						/**
						 * Current workflow
						 */
						_self.workflow = WorkflowsService.getWorkflow('current');

						/**
						 * Reference to the Workflows and Application Services that expose
						 * methods that are used by the template
						 */
						_self.applicationService = ApplicationService;
						_self.workflowsService = WorkflowsService;

					
						/**
						 * Current values of the edit input form
						 */
						_self.input_edit = {
							name : _self.workflow.name,
							description : _self.workflow.description,
							tags : ApplicationService.tagsToNgTags(_self.workflow.tags),
							execution_frequency : _self.workflow.execution_frequency
						};

						/**
						 * Original values that were fetched form the backend when loading
						 * the page.
						 * 
						 * @private
						 */
						var original_input_edit = angular.copy(_self.input_edit);

						/**
						 * Save the new information
						 * 
						 */
						_self.saveWorkflowInfos = function() {
							// make a deep copy and upgrade the new information
							var new_workflow = angular.copy(_self.workflow);
							new_workflow.name = _self.input_edit.name;	
							new_workflow.description = _self.input_edit.description;
							new_workflow.execution_frequency = _self.input_edit.execution_frequency;
							new_workflow.tags = _self.input_edit.tags;
							WorkflowsService.mapScheduleSettingsToBackend(new_workflow);

							WorkflowsService.saveWorkflow(new_workflow).then(function(id) {
								ApplicationStateService.addMessage('The new information has been saved.', 'success')								
							});
						};

						/**
						 * Reset the edit form to its initial state
						 */
						_self.resetEditionForm = function() {
							_self.input_edit = angular.copy(original_input_edit);
						};

						/**
						 * Tells whether the edit form has been modified.
						 */
						_self.editFormIsPristine = function() {
							return angular.equals(_self.input_edit, original_input_edit);
						}

						/**
						 * Trigger workflow deletion
						 * 
						 */
						_self.deleteWorkflow = function() {
							WorkflowsService.deleteWorkflow(_self.workflow.id).then(function() {
								ApplicationStateService.addMessage('Workflow #' + _self.workflow.id + ' has been deleted.', 'success', false);
								$location.path('/');								
							});
						};

						/**
						 * Cancel the workflow deletion (uncheck the confirmation checkbox)
						 */
						_self.cancelDeletion = function() {
							_self.input_deletion.confirm_deletion = false;
						};
						

						
						
					} ]);

}());
