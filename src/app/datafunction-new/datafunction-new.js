/**
 * @file datafunction-new.js
 * 
 * workflowEditor.datafunctionNew module
 * 
 * Controller used to create a new data function, with optional parameters.
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular
			.module(
					'workflowEditor.dataFunctionNew',
					[ 'ngTagsInput', 'workflowEditor.applicationStateService',
							'workflowEditor.applicationService',
							'workflowEditor.workflowsService' ])
			.controller(
					'NewDataFunctionController',
					[	'$location', 'ApplicationStateService',	'ApplicationService',
							function($location, ApplicationStateService, ApplicationService) {

								var _self = this;

								/**
								 * Reference to the Application Service that exposes methods
								 * that are used by the template
								 */
								_self.applicationService = ApplicationService;

								/**
								 * Current form input
								 */
								_self.input = {
									name : '',
									description : '',
									tags : [],
									type : 0, // FIXME enum?
									method : 0, // FIXME enum?
									uri : '',
									parameters : []
								};
								// original state of the form
								var empty_input = angular.copy(_self.input);

								/**
								 * Add a new parameter
								 */
								_self.addParameter = function() {
									_self.input.parameters.push({
										type : 'string',
										name : '',
										defaultValue : '',
										mandatory : false,
									});
								};

								/**
								 * Remove a parameter
								 */
								_self.removeParameter = function(index) {
									_self.input.parameters.splice(index, 1);
								};

								/**
								 * Trigger the creation of the new workflow
								 */
								_self.createNewDataFunction = function() {
									ApplicationService.saveDataFunction(_self.input)
									.then(function(new_id) {
										ApplicationStateService.addMessage('Your new data function has been successfully created. You can now use it.', 'success', false);
										// redirect to the listing of data functions
										$location.path('/application/datafunction/list');
									});
								};

								/**
								 * Reset the form to its original state (empty)
								 */
								_self.resetCreationForm = function() {
									_self.input = angular.copy(empty_input);
								}

							} ]);

}());
