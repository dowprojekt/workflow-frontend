/**
 * @file workflow-new.js
 * 
 * workflowEditor.new module
 * 
 * Responsible for rendering a workflow creation form
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular.module('workflowEditor.new',
			[ 'ngTagsInput', 'workflowEditor.applicationStateService', 'workflowEditor.applicationService', 'workflowEditor.workflowsService' ])

	.controller('NewWorkflowController',
			[ '$q', '$location', 'ApplicationStateService', 'ApplicationService', 'WorkflowsService', 
			  function($q, $location, ApplicationStateService, ApplicationService, WorkflowsService) {

				var _self = this;

				/**
				 * Reference to the Application Service that exposes methods that are used by the template
				 */
				_self.applicationService = ApplicationService;

				/**
				 * Current form input
				 */
				_self.input = {
					name: '',
					description: '',
					tags: [],
					execution_frequency: ''
				};
				// original state of the form
				var empty_input = angular.copy(_self.input);
				
				/**
				 * Trigger the creation of the new workflow
				 */
				_self.createNewWorkflow = function() {				
					// transform schedule
					WorkflowsService.mapScheduleSettingsToBackend(_self.input);

					// and submit to backend
					WorkflowsService.saveWorkflow(_self.input).then(function(new_id) {
						ApplicationStateService.addMessage('Your new workflow has been successfully created. You can now configure it.', 'success', false);
						// redirect to the configuration page
						$location.path('/workflow/' + new_id + '/configure-steps');
					});
				};
				
				/** 
				 * Reset the form to its original state (empty)
				 */
				_self.resetCreationForm = function() {
					_self.input = angular.copy(empty_input);
				}

			} ]);

}());
