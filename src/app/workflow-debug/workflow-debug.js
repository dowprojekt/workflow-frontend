/**
 * @file workflow-debug.js
 * 
 * workflowEditor.debug module
 *
 * Display the internal data model of the workflow.
 * 
 * FIXME TODO in the future, display debugging information coming from the backend 
 * (such as feedback from SpringBatch)
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {
	angular.module('workflowEditor.debug', [ 'workflowEditor.workflowsService' ])

	.controller('DebugWorkflowController',
			[ 'WorkflowsService', '$location', function(WorkflowsService, $location) {
				var _self = this;

				/**
				 * Current workflow
				 */
				_self.workflow = WorkflowsService.getWorkflow('current');

			} ]);

}());
