/**
 * @file workflow-listing.js
 * 
 * workflowEditor.list module
 * 
 * Responsible for rendering a summary of the workflows that exist in the system
 * 
 * This page consists of a list of workflows and a form for filtering the
 * display of this list. There are also direct action buttons for performing
 * tasks on workflows.
 * 
 * @@source_header
 * 
 */

'use strict';

(function() {

	angular.module('workflowEditor.list', [ 'workflowEditor.workflowsService' ])

	/**
	 * ListController:
	 */
	.controller('ListController',
			[ 'WorkflowsService', function(WorkflowsService) {

				var _self = this;

				/**
				 * Reference to the workflow service object that will be using
				 * extensively
				 */
				_self.workflowsService = WorkflowsService;

				/**
				 * Form input variables
				 */
				_self.input = {
					search : '',
					status : ''
				};
				_self.applied_input_values = angular.copy(_self.input);

				/**
				 * Do apply the filter
				 */
				_self.filterList = function() {
					_self.applied_input_values = angular.copy(_self.input);
				};

				/**
				 * Reset the filter to empty values
				 */
				_self.resetListFilter = function() {
					_self.input = angular.copy(_self.applied_input_values);
				};

				/**
				 * Tells whether the filter form is pristine or not
				 */
				_self.filterFormIsPristine = function() {
					return angular.equals(_self.input, _self.applied_input_values);
				}

			} ]);

}());
