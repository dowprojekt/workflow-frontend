/**
 * @file workflow-configure.js
 * 
 * Page for configuring workflow steps. This page allows to:
 * - view the sequence of workflow steps
 * - add new steps
 * - reorder steps
 * - edit steps
 * 
 * @@source_header
 * 
 */
'use strict';

(function() {
	angular.module(
			'workflowEditor.configure',
			[ 'workflowEditor.applicationStateService',
			  	'workflowEditor.applicationService',
					'workflowEditor.workflowsService' ])

	.controller(
			'ConfigureWorkflowController',
			[ 'ApplicationStateService', 'ApplicationService', 'WorkflowsService',
					function(ApplicationStateService, ApplicationService, WorkflowsService) {

						var _self = this;

						/**
						 * Tells what part of the page to show
						 */
						_self.view = 'global';

						/**
						 * Defines the subject of step creation or step edition: - on
						 * creation: the subject is set to the step *at which* we must
						 * insert the newly created step; other steps will be pushed lower - on edition: the subject is set to
						 * the index of the step being edited
						 */
						_self.subject = false;
						
						/**
						 * Used to display or hide the step removal confirmation buttons
						 */
						_self.display_remove_confirmation = [];
						
						/**
						 * The local copy of the workflow object that is being edited
						 */
						_self.input = {};
						
						/**
						 * Input object for creation of new step
						 */
						_self.input_new = {};
						
						/**
						 * Form input variables
						 */
						_self.df_filter_input_values = {
							search : '',
							service_type : ''
						};
						_self.df_filter_input_applied_values = angular.copy(_self.df_filter_input_values);

						/**
						 * Do apply the data function filter
						 */
						_self.filterDataFunctionList = function() {
							_self.df_filter_input_applied_values = angular.copy(_self.df_filter_input_values);
						};

						/**
						 * Reset the data function filter to empty values
						 */
						_self.resetDataFunctionListFilter = function() {
							_self.df_filter_input_values = angular.copy(_self.df_filter_input_applied_values);
						};

						/**
						 * Tells whether the data function filter form is pristine or not
						 */
						_self.dataFunctionFilterFormIsPristine = function() {
							return angular.equals(_self.df_filter_input_values, _self.df_filter_input_applied_values);
						}
						
						
						/**
						 * A helper function to setup the instance variables of this page
						 */
						_self.init = function() {
							_self.workflow = WorkflowsService.getWorkflow('current');
							angular.copy(_self.workflow, _self.input);
						}
						_self.init();
						
						
						/**
						 * Reference to Workflows and Application Services that will be used from within the
						 * tepmlate
						 */
						_self.workflowsService = WorkflowsService;
						_self.applicationService = ApplicationService;
												
						/**
						 * Launch the UI for adding a new step
						 */
						_self.addStep = function(where) {
							_self.subject = where;
							_self.view = 'new';
						};
						
						/**
						 * Launch the UI for editing a step
						 */
						_self.editStep = function(which) {
							_self.subject = which;
							_self.view = 'edit';
						};
						
						
						/**
						 * Save the new configuration that is stored in the input_new object
						 */
						_self.saveNew = function() {
							
							var new_step = {actions: [{
								description: _self.input_new.description,
								dataFunction: angular.copy(ApplicationService.application.dataFunctions[_self.input_new.chosen_df])
							}]};
							
							// replace parameters values with the ones we have collected
							for (var i=0; i < new_step.actions[0].dataFunction.parameters.length; ++i) {
								new_step.actions[0].dataFunction.parameters[i].value = _self.input_new.parameters[i].value;
								new_step.actions[0].dataFunction.parameters[i].description = _self.input_new.parameters[i].description;
							}
							
							// inject into the sequence of steps
							_self.input.steps.splice(_self.subject, 0, new_step);
							
							// and permanently save the current input 
							_self.save();
						};
						
						/**
						 * Save the new configuration that is stored in the input variable
						 */
						_self.save = function() {
							WorkflowsService.saveWorkflow(_self.input).then(function(id) {
								_self.init();
								ApplicationStateService.addMessage('The new information has been saved.', 'success');
							});
							
							_self.subject = false;
							_self.view = 'global';
						};
						
						/**
						 * Reset the configuration to the state it was initially when loading the page
						 */
						_self.cancel = function() {
							angular.copy(_self.workflow, _self.input);
							_self.subject = false;
							_self.view = 'global';
						};
						
						/**
						 * Move a step upper
						 */
						_self.moveUp = function(index_to_move) {
							if (!_self.input.steps[index_to_move]) {
								ApplicationStateService.crash('Step index does not exist.');
							}
							var tmp = angular.copy(_self.input.steps[index_to_move-1]);
							_self.input.steps[index_to_move-1] = _self.input.steps[index_to_move];
							_self.input.steps[index_to_move] = tmp;
						
							WorkflowsService.saveWorkflow(_self.input).then(function(id) {
								_self.init();
								ApplicationStateService.addMessage('The new information has been saved.', 'success');
							});
							
						};
						
						/**
						 * Move a step lower
						 */
						_self.moveDown = function(index_to_move) {
							if (!_self.input.steps[index_to_move]) {
								ApplicationStateService.crash('Step index does not exist.');
							}
							var tmp = angular.copy(_self.input.steps[index_to_move+1]);
							_self.input.steps[index_to_move+1] = _self.input.steps[index_to_move];
							_self.input.steps[index_to_move] = tmp;
							
							WorkflowsService.saveWorkflow(_self.input).then(function(id) {
								_self.init();
								ApplicationStateService.addMessage('The new information has been saved.', 'success');
							});
						};
						
						/**
						 * Remove a step
						 */
						_self.removeStep = function(index) {
							_self.input.steps.splice(index, 1);
							WorkflowsService.saveWorkflow(_self.input).then(function(id) {
								_self.display_remove_confirmation[index] = false;
								_self.init();
								ApplicationStateService.addMessage('The new information has been saved.', 'success');
							});
						};
						
				
					} ]);

}());
