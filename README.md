# KTI-DOW Workflow Editor front end #

## Project description

This workflow editor is part of the Linked Data Orchestration Workbench (DOW) KTI-CTI project.
The original description of work follows hereafter:

> Business applications read and write data from/to a considerable number of different data sources,
> which typically requires a series of data extraction and transformation steps: existing data sources
> must be transformed (lifted) to RDF(S) first in order to be accessible as Linked Data, and data sets
> that are already in a semantic format may need to be integrated explicitly in case different
> ontologies have been used during modeling, which now need to be mapped to one another before
> being able to produce a unified data set. Currently, these data manipulation activities have to be
> programmed individually for each application. To simplify this task for data integrators/modelers,
> we will therefore develop a graphical tool for the modeling of Linked Data integration workflows.
> The tool will allow to to graphically define sequences (workflows) of data manipulation steps that
> gradually combine and transform different sources of input data into a customized Linked Data set
> required for a specific application. The target data set is in Linked Data format with a unifying RDF
> schema, while the input data sources may have different schemas. Once defined, transformation
> steps will be materialized as Web service complete with a formalized service interface description,
> so that they can be stored and reused for different application scenarios. Compared to the 
> state-of-the-art, this graphical modeling tool will therefore simplify and speed up 
> low-level data integration tasks.

## Solution architecture

### General solution design

The workflow editor consists 3 independent software components:

 1. A graphical web-based frontend that will allow creating and editing workflows in a user-friendly manner. This is the content of the present repository.
 2. A JAVA backend responsible for storing workflows and deploying them on the the third component.
 3. A SpringBatch processing engine that will execute the workflows.

### Frontend implementation

#### Code organization

In this Section, we will explain how the code of the solution is organized. We focus on the files related to the application logic only. Other directories and files are omitted. We will use AngularJS concepts and the reader can obtain documentation from the website of this framework.

The simplified structure of the ./src/ directory is depicted hereafter:

````
|── src/
    ├── app/
    │   ├── common/
    │   │   ├── controllers/
    │   │   └── services/
    │   ├── datafunction-edit/
    │   ├── ...
    │   ├── (more tasks here)
    │   ├── ...
    │   ├── workflow-simulate/
    |   ├── config.js
    │   └── workflow-editor.js
    ├── index.html
    └── scss/
        └── workflow-editor.scss
````

- `src/index.html`: this is the entry point of the web application and where the main AngularJS workflow editor module is instantiated.

- `src/app/config.js`: this file contains application-wide settings.

- `src/app/workflow-editor.js`: definition of the main AngularJS workflow editor module. This module register the different routes of the application and initially contacts the workflow editor backend for retrieving application settings, user information and available workflows by launching the relevant services. The other directories in the src/app/ directory contains the different AngularJS modules that compose the web application.

- `src/app/common/`: AngularJS modules (services and controllers) that are common to more than one task or pertaining to the whole application. The content of this directory is detailed later in this document.

- `src/app/<task>/`: AngularJS modules specific to a task. For example, the page displaying the listing of existing workflows will require a module consisting of a Controller used to render the list and of a template that will define the layout of the workflows to be displayed.

- `src/scss/`: this directory contains Sass files that are used to generate CSS files specific to our web application. The workflow editor design is implemented by using (a) the Bootstrap framework and (b) the UI Bootstrap AngularJS module. These custom CSS files are necessary for fine-tuning the layout rendered to the user.

#### Application logic

Following AngularJS best practices, the workflow editor frontend does implement various services stored in `src/app/common/services/` that are responsible for constructing and updating the data model of the application:

- *ApplicationStateService*: this service keeps track of the general state of the application (e.g. whether the application has crashed) and is used by the ApplicationStateController to inform the user when necessary.

- *ApplicationService*: this service keeps track of application-wide information, such as available data functions. 

- *UserService*: this service is responsible for handling user-related settings and actions.

- *WorkflowsService*: this service manages the different workflows that can be edited by the data engineer using the application.

- *BackendService*: service responsible for communicating with the workflow editor backend through the REST API of the solution. ApplicationService, UserService and WorkflowsService use BackendService to abstract communications.

Additionally, the data model is rendered to the end-user through controllers, taking benefit of the two-way data binding provided by AngularJS. There are 2 types of controllers: task-specific controllers that create page contents and common controllers, stored in `src/app/common/controllers/`, render visual elements that are common to more than one page. For example, the MenuController common controller will render the menu for accessing the different pages of the application.

If a controller needs an HTML template, it is stored in the same directory and with the same name, but with its extension changed to tpl.html. For example, the MenuController is defined in `src/app/common/controllers/menu.js` and its associated template is stored in `src/app/common/controllers/menu.tpl.html`.

More information about the implementation details can be found in the comments of the source files.


## Building and packaging

### Prerequisites

The building process relies on npm for downloading and installing required dependencies. Once all dependencies have been installed, the project can be built using different Grunt tasks. Therefore, to prepare your development environment to build the workflow editor frontend, you must:

 1. Download the sources of the project
 1. Install a recent version of npm. Version 1.4.28 is known to work. Older versions may not be sufficient.
 1. In the sources directory, run `npm install`. The list of installed packages is described in the package.json file.
 1. Install sass: `gem install sass` (Depends on ruby)

This web application has been developed on a Linux Ubuntu 14.04LTS system with an updated version of npm. Other build environments have not been tested.

All project dependencies are installed locally. This means that any subsequent command must be called from within the context of the sources directory.

### Third parties contributions and licenses 

When the project is built via the Grunt tasks mentioned earlier, some dependencies are downloaded and stored in the ./src/contrib/ directory (JavaScript libraries, CSS frameworks, art works). This web dependencies management is performed by bower, and the external contributions are described in the bower.json.

All these third-party components are released under commercial-friendly and open-source licenses, such as the MIT license. 

### Application configuration

Change the configuration of the application by editing the src/app/config.js file:

 - BACKEND_ENTRY_POINT: the URL of the workflow backend, *with* trailing slash.
 - EXPECTED_API_VERSION: the exact API version that the frontend is ready to accept to function properly. 

### Grunt tasks

The following Grunt tasks are available via the ./grunt command (an alias to the locally installed Grunt):


	$ ./grunt build-dev
	Builds the current sources and exit.


	$ ./grunt build-dev-live
	Builds the current sources, opens a browser and delivers the 
	application from a locally created web server; then watch for 
	changes: when a file is modified, rebuild the sources and reload 
	the browser. 


	$ ./grunt run-unit-tests
	Runs all unit tests and exit. See Section 4.3.1 of this document 
	for more details.


	$ ./grunt run-unit-tests-live 
	Runs all unit tests, then watch for changes: when a file is 
	modified, reruns all unit tests.

	$ ./grunt run-e2e-tests
	Runs all end-to-end tests and exit. Note that a Selenium server 
	must be installed and running. 


	$ ./grunt build-package
	Builds a clean package ready for production deployment. The package
	is created in the current directory and is named after the version
	number (e.g. ./workflow-frontend-v1.0.2/). The version number can
	be changed by modifying the version string in the package.json file.

The Gruntfile.js file contains all Grunt tasks and subtasks and can be inspected for fully understanding the functioning of the above documented tasks.

### Testing

Unit tests are defined in *.spec.js files. They are written in Jasmine and executed with the Karma test engine.

End-to-end (or behavioral) tests are defined in ./resources/e2e-testing/. They are written in Jasmine and executed with the Selenium standalone test server.

### Packages

The `./grunt build-package` command creates a package that is ready for production environment.

A package is slightly different from a development build: 

 - Some strings are replaced with predefined values (e.g. @@pkg_version). The content of ./resources/documentation/source-header.txt is of special interest, as it contains a text that will be inserted at the top of most files.
 - Useless and development files are removed from the produced package.

## Additional resources

### How to get a recent version of npm on Ubuntu?

	sudo apt-get install python-software-properties
	sudo add-apt-repository ppa:chris-lea/node.js  
	sudo apt-get update  
	sudo apt-get install nodejs
	

